import java.util.Scanner;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Ejercicio {
    public static void main(String[] args) {
        System.out.println("--------------------------------------------------------------------------------------- \n");
        System.out.println(" 1) Declara dos variables numéricas (con el valor que desees), " +
                "muestra por consola la suma, resta,\n" +"multiplicación, división y módulo (resto de la división). \n");

        int numero1 = 10;
        int numero2 = 5;

        System.out.println("---------------------------------------RESULTADOS--------------------------------------");
        //suma
        System.out.println("La suma de "+numero1+" + "+numero2+" = "+(numero1+numero2));
        //resta
        System.out.println("La resta de "+numero1+" - "+numero2+" = "+(numero1-numero2));
        //multiplicación
        System.out.println("La multiplicación de "+numero1+" * "+numero2+" = "+(numero1*numero2));
        //división
        System.out.println("La división de "+numero1+" / "+numero2+" = "+(numero1/numero2));
        //modulo
        System.out.println("El módulo de "+numero1+" mod "+numero2+" = "+(numero1%numero2));
        //resta
        System.out.println("---------------------------------------------------------------------------------------");
        System.out.println("2) Convierte un texto en mayúsculas");
        String texto = "tres tristes tigres";
        System.out.println("RESULTADO: ");
        System.out.println(texto+" = "+texto.toUpperCase());
        System.out.println("---------------------------------------------------------------------------------------");
        System.out.println("3) Declara un String que contenga tu nombre, después muestra un mensaje\n" +
                "de bienvenida por consola. Por ejemplo: si introduzco «Fernando», me aparezca\n" +
                "«Bienvenido Fernando».");
        String nombre = "Fernando";
        System.out.println("RESULTADO: ");
        System.out.println("<<Bienvenido "+nombre+">>");
        System.out.println("--------------------------------------------------------------------------------------------");

        System.out.println("4) Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los dos." +
                "Si son iguales indicarlo también. Ves cambiando los valores para comprobar que funciona.");
        int num1 = 4;
        int num2 = 9;

        if (num1 > num2) {
            System.out.println("El numero mayor es: " + num1);
        } else {
            if (num2 > num1) {
                System.out.println("El numero mayor es: " + num2);
            } else {
                System.out.println("Los números son iguales");
            }
        }

        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("5) Haz una aplicación que calcule el área de un círculo(pi*R2). El radio se pedirá por teclado " +
                "(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el método pow de Math.");

        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese el radio del circulo");
        double radio = Double.parseDouble(leer.nextLine());
        double area = PI*pow(radio,2);
        System.out.println("El área del circulo de radio "+radio+" es:"+area);

        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("6) Lee un número por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, también debemos indicarlo");
        Scanner lectura = new Scanner(System.in);
        System.out.println("Ingrese un número por favor");
        int $numero = Integer.parseInt(lectura.nextLine());
        if ($numero % 2 == 0){
            System.out.println("Es divisible entre 2");
        }else{
            System.out.println("No es divisible entre 2");
        }

        System.out.println("7) Lee un número por teclado y muestra por consola, el carácter al que pertenece en la tabla ASCII." +
                "Por ejemplo: si introduzco un 97, me muestre una a.");
        Scanner lec = new Scanner(System.in);
        int num = lec.nextInt();
        char trans = (char) num;
        System.out.println("El código ASCII de: "+num+" es= "+trans);

        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("8) Modifica el ejercicio anterior, para que en lugar de pedir un número, " +
                "pida un carácter (char) y muestre su código en la tabla ASCII.");
        Scanner lect = new Scanner(System.in);
        char letra = lect.nextLine().charAt(0);
        int numTrans = letra;

        System.out.println("La letra <<"+letra+">> equivale en ASCII a = "+numTrans);


        System.out.println("9) Lee un número por teclado que pida el precio de un producto (puede tener decimales) " +
                "y calcule el precio final con IVA. El IVA sera una constante que sera del 21%.");
        System.out.println("--------------------------------------------------------------------------------------------");
        Scanner leerNumero = new Scanner(System.in);
        double precio = leerNumero.nextDouble();
        System.out.println("Precio sin IVA: "+precio);
        double precioFinal = precio + (precio*0.21);
        System.out.println("El total a pagar es:"+precioFinal);

        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("Lee un número por teclado y comprueba que este numero es mayor o igual que cero, " +
                "si no lo es lo volverá a pedir (do while), después muestra ese número por consola.");
        Scanner numLect = new Scanner(System.in);
        int numPositivo;
        do{
            System.out.println("Ingresa un número positivo por favor");
            numPositivo = numLect.nextInt();
        }while (numPositivo <= 0);
        System.out.println("El numero ingresado es: "+numPositivo);



    }
}